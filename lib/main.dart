import 'dart:js_util';

import 'package:flutter/material.dart';
import 'dart:async';

import 'dart:js' as js;

import 'dart:html' as html;
import 'dart:html';
import 'dart:convert';
var videos=['video1.mp4','video2.mp4','video3.mp4'];
class DynamicMatrix {
  List<List<int>> _data = [];

  // Set a value at specific coordinates
  void setValue(int row, int col, int value) {
    // Ensure the matrix has enough rows
    while (row >= _data.length) {
      _data.add([]); // Add an empty growable list for the new row
    }

    // Ensure the specified row has enough columns
    while (col >= _data[row].length) {
      _data[row].add(0); // Add default value for new columns
    }

    _data[row][col] = value; // Set the value
    print(_data.map((row) => row.join(' ')).join('\n'));
  }

  // Get a value at specific coordinates
  int getValue(int row, int col) {
    if (row >= _data.length || col >= _data[row].length) {
      return 0; // Return a default value if out of bounds
    }
    return _data[row][col]; // Return the value at the specified coordinates
  }

  // Get the maximum number of columns in the current matrix


  // Optionally, print the matrix for visualization
  @override
  String toString() {
    return _data.map((row) => row.join(' ')).join('\n');
  }
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Editable Table with Search, Filters, and Selection'),
        ),
        body: TablePage(),
      ),
    );
  }
}

class TablePage extends StatefulWidget {
  @override
  _TablePageState createState() => _TablePageState();
}

class _TablePageState extends State<TablePage> {

  final Map<String, int> vars =  {
    'nombre_senior': 0,
    'nombre_Adult': 0,
    'nombre_jeune_Adult': 0,
    'nombre_jeune': 0,
    'nombre_moyen_age': 0,
    'moyen_senior': 0,
    'moyen_Adult': 0,
    'moyen_jeune_Adult': 0,
    'moyen_jeune': 0,
    'moyen_moyen_age': 0,
    'nombre_homme': 0,
    'nombre_femme': 0,
    'moyen_homme': 0,
    'moyen_femme': 0,
    'nombre_heureux': 0,
    'nombre_content': 0,
    'nombre_neutre': 0,
    'nombre_mecontent': 0,
    'moyen_heureux': 0,
    'moyen_content': 0,
    'moyen_neutre': 0,
    'moyen_mecontent': 0,
    'min_concentration': 0,
    'moyen_concentration': 0,
    'nombre_temps_attention': 0,
    'nombre_temps_inattention': 0,
    'nombre_ots': 0,
    'nombre_attention': 0,
    'nombre_inattention': 0,
    'moyen_attention': 0,
    'moyen_inattention': 0,
    'nombre_concentration': 0,
    'moyen_sertitude_genre': 0,
    'taux_observation': 0,
    'taux_conversion_homme': 0,
    'taux_conversion_femme': 0,
    'taux_conversion_senior': 0,
    'taux_conversion_Adult': 0,
    'taux_conversion_jeune_Adult': 0,
    'taux_conversion_jeune': 0,
    'taux_conversion_heureux': 0,
    'taux_conversion_content': 0,
    'taux_conversion_mecontent': 0,
    'taux_conversion_neutre': 0,
    'nombre_inattention_senior': 0,
    'nombre_inattention_Adult': 0,
    'nombre_inattention_jeune_Adult': 0,
    'nombre_inattention_jeune': 0,
    'nombre_inattention_homme': 0,
    'nombre_inattention_femme': 0,
    'nombre_inattention_heureux': 0,
    'nombre_inattention_content': 0,
    'nombre_inattention_neutre': 0,
    'nombre_inattention_mecontent': 0,
    'nombre_attention_senior': 0,
    'nombre_attention_Adult': 0,
    'nombre_attention_jeune_Adult': 0,
    'nombre_attention_jeune': 0,
    'nombre_attention_homme': 0,
    'nombre_attention_femme': 0,
    'nombre_attention_heureux': 0,
    'nombre_attention_content': 0,
    'nombre_attention_neutre': 0,
    'nombre_attention_mecontent': 0,
    'nombre_temps_attention_senior': 0,
    'nombre_temps_attention_Adult': 0,
    'nombre_temps_attention_jeune_Adult': 0,
    'nombre_temps_attention_jeune': 0,
    'nombre_temps_attention_homme': 0,
    'nombre_temps_attention_femme': 0,
    'nombre_temps_attention_heureux': 0,
    'nombre_temps_attention_content': 0,
    'nombre_temps_attention_neutre': 0,
    'nombre_temps_attention_mecontent': 0,
    'nombre_temps_inattention_senior': 0,
    'nombre_temps_inattention_Adult': 0,
    'nombre_temps_inattention_jeune_Adult': 0,
    'nombre_temps_inattention_jeune': 0,
    'nombre_temps_inattention_homme': 0,
    'nombre_temps_inattention_femme': 0,
    'nombre_temps_inattention_heureux': 0,
    'nombre_temps_inattention_content': 0,
    'nombre_temps_inattention_neutre': 0,
    'nombre_temps_inattention_mecontent': 0
  };

  final Map<String, int> varsc = {
    // Define your cumulative indicators and values here
    'cumul_attention': 0,
    'cumul_inattention': 0,
    'cumul_temps_attention': 0,
    'cumul_temps_inattention': 0,
    'cumul_ots': 0,
    'cumul_temps_ots': 0,
    'cumul_senior': 0,
    'cumul_Adult': 0,
    'cumul_jeune_Adult': 0,
    'cumul_jeune': 0,
    'cumul_moyen_age': 0,
    'cumul_heureux': 0,
    'cumul_content': 0,
    'cumul_neutre': 0,
    'cumul_mecontent': 0,
    'cumul_homme': 0,
    'cumul_femme': 0
    // (Add all your cumulative indicators as needed)
  };

  final List<String> operators = [
    '+', '-', '*', '/', 'AND', 'OR', 'NOT', 'XOR', '>', '<', '>=', '<=', '==', '!='
  ];

  bool showIndicators = true;
  String filterText = '';
  Set<String> selectedRows = {};
  List<dynamic> _dirs = [];

  List<Map<String, String>> _getData() {
    List<Map<String, String>> data = [];
    Map<String, int> combined = {...vars, ...varsc};

    combined.forEach((key, value) {
      if (showIndicators) {
        if (vars.containsKey(key)) {
          data.add({'name': key, 'type': 'Indicator'});
        }
      } else {
        if (operators.contains(key)) {
          data.add({'name': key, 'type': 'Operator'});
        }
      }
    });

    if (!showIndicators) {
      operators.forEach((operator) {
        data.add({'name': operator, 'type': 'Operator'});
      });
    }

    return data;
  }

  void select(Map item) {
    selectedRows.add(item['name']!);
  }

  void deselect(Map item) {
    selectedRows.remove(item['name']!);
  }

  void _selectAll() async{

    setState(() {
      selectedRows.clear();
      final data = _getData();
      data.forEach((item) {
        if (item['name']!.toLowerCase().contains(filterText.toLowerCase())) {
          selectedRows.add(item['name']!);
          select(item);
        }
      });
    });
  }

  void _deselectAll() {
    setState(() {
      selectedRows.clear();
    });
  }

  void _navigateToManageRows() {

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ManageRowsScreen(
          selectedIndicators: selectedRows.where((item) => vars.containsKey(item)).toList(),
          selectedOperators: selectedRows.where((item) => operators.contains(item)).toList(),
        ),
      ),
    );
  }
  @override
  void initState() {
    super.initState();
    window.onMessage.listen((MessageEvent event) {
      var text=event.data.toString().replaceAll('&#39;', "'");
      text = text.replaceAll(RegExp(r"[\[\]' ]"), '');

      // Split the string by commas and trim any whitespace
     videos= text.split(',').map((e) => e.trim()).toList();

      print(videos);
    });
    Timer.periodic(const Duration(seconds: 15), (timer) async {
      final iframe = html.IFrameElement()
        ..src = 'https://qias.tv:5000/send/send' // Set the source of the iframe
        ..id = 'myIframe'
        ..style.width = '100%'
        ..style.height = '500px'
        ..style.border = 'none'; // You can style it as needed

      // Append the iframe to the body or a specific element
      html.document.body!.append(iframe);

print('done');
      // await  getListvideos();
    });

  }
  void readIframeContent() {
    // Access the iframe
    final iframe = html.document.getElementById('myIframe') as html.IFrameElement;
    if (iframe != null) {
      final innerDoc = iframe.contentWindow.toString();
      print(innerDoc);

    }
  }

  Future<void> _fetchDirs() async {
    try {
      var result =  promiseToFuture(js.context.callMethod('fetchDirs'));
   //  result.then((value) => print(value));
     // print("doneeee");
     // setState(() {
       // _dirs = result;
     // });
    } catch (error) {
      print('Error fetching directories: $error');
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Map<String, String>> data = _getData();
    List<Map<String, String>> filteredData = data
        .where((item) => item['name']!.toLowerCase().contains(filterText.toLowerCase()))
        .toList();

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: TextField(
            decoration: InputDecoration(
              labelText: 'Search',
              border: OutlineInputBorder(),
            ),
            onChanged: (value) {
              setState(() {
                filterText = value;
              });
            },
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Show Indicators'),
            Switch(
              value: showIndicators,
              onChanged: (value) {
                setState(() {
                  showIndicators = value;
                });
              },
            ),
            Text('Show Operators'),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: _selectAll,
              child: Text('Select All'),
            ),

            SizedBox(width: 16),
            ElevatedButton(
              onPressed: _deselectAll,
              child: Text('Deselect All'),
            ),
            SizedBox(width: 16),
            ElevatedButton(
              onPressed: _navigateToManageRows,
              child: Text('Manage Selected Rows'),
            ),
          ],
        ),
        Expanded(
          child: Scrollbar(
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: DataTable(
                  columns: [
                    DataColumn(label: Text('Select')),
                    DataColumn(label: Text('Name')),
                    DataColumn(label: Text('Type')),
                  ],
                  rows: filteredData.map((item) {
                    final isSelected = selectedRows.contains(item['name']);
                    return DataRow(
                      cells: [
                        DataCell(
                          Checkbox(
                            value: isSelected,
                            onChanged: (checked) {
                              setState(() {
                                if (checked == true) {
                                  selectedRows.add(item['name']!);
                                  select(item);
                                } else {
                                  selectedRows.remove(item['name']!);
                                  deselect(item);
                                }
                              });
                            },
                          ),
                        ),
                        DataCell(Text(item['name']!)),
                        DataCell(Text(item['type']!)),
                      ],
                    );
                  }).toList(),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class ManageRowsScreen extends StatefulWidget {
  final List<String> selectedIndicators;
  final List<String> selectedOperators;

  ManageRowsScreen({
    required this.selectedIndicators,
    required this.selectedOperators,
  });

  @override
  _ManageRowsScreenState createState() => _ManageRowsScreenState();
}

class _ManageRowsScreenState extends State<ManageRowsScreen> {

  List<String> columns = ['Indicator/Operator'];
  List<String> columnTypes = ['Indicator']; // Keeps track of the type for each column
  List<List<String?>> columnValues = [[]]; // Values for each column
  List<int> numberInputs = [0]; // Keeps track of number input values
  DynamicMatrix matrix = DynamicMatrix();
  void _addColumn() {
    setState(() {
      columns.add('Indicator/Operator');
      columnTypes.add('Indicator'); // Default to showing indicators
      columnValues.add(List.filled(widget.selectedIndicators.length, null)); // Initialize with null for new column
     numberInputs.add(0); // Initialize number input
      matrix.setValue(0, 0, 0);
      matrix.setValue(4, 4, 0);
      columnTypes.add('Video');
      columnValues.add(List.filled(videos.length, null));
    });
  }

  void _removeColumn() {
    if (columns.length > 1) {
      setState(() {
        columns.removeLast();
        columnTypes.removeLast();
        columnValues.removeLast();
        numberInputs.removeLast();
      });
    }
  }
  void _generateJavaScriptCode() {
    StringBuffer jsCode = StringBuffer();
    jsCode.writeln('const actions = [];');

    for (int rowIndex = 0; rowIndex < widget.selectedIndicators.length; rowIndex++) {
      String indicator = widget.selectedIndicators[rowIndex];
      String condition = '';

      for (int colIndex = 0; colIndex < columns.length; colIndex++) {
        if (columnTypes[colIndex] == 'Number') {
          int value = matrix.getValue(rowIndex, colIndex);
          print(value);
          print("vvvvvvvvv");
          condition += ' ${value > 0 ? '' : ''} $value && ';
        } else {
          String? operator = columnValues[colIndex][rowIndex];
          if (operator != null) {
            condition += '$indicator $operator ';
          }
        }
      }

      // Remove the last ' && '
      if (condition.isNotEmpty) {
        condition = condition.substring(0, condition.length - 4);
        jsCode.writeln('if ($condition) {');
        jsCode.writeln('  actions.push("$indicator action executed");');
        jsCode.writeln('}');
      }
    }

    // Print or display the generated code
    print(jsCode.toString());
    // Optionally, show it in a dialog
    _showGeneratedCodeDialog(jsCode.toString());
  }

  void _showGeneratedCodeDialog(String jsCode) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Generated JavaScript Code'),
          content: SingleChildScrollView(
            child: Text(jsCode),
          ),
          actions: [
            TextButton(
              child: Text('Close'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _changeMatrixInput(int ii,int jj, int change) {
    setState(() {
      var ini=0;
      try{ini=matrix.getValue(ii, jj);}catch(e){};
      var inic;
      inic=ini+change;

      matrix.setValue(ii, jj, inic);

    });
  }


    @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Manage Selected Rows'),
      ),
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: (){


                },
                child: Text('test'),
              ),
              ElevatedButton(
                onPressed: _addColumn,
                child: Text('Add Column'),
              ),
              SizedBox(width: 16),
              ElevatedButton(
                onPressed: _removeColumn,
                child: Text('Remove Column'),
              ), SizedBox(width: 16),
              ElevatedButton(
                onPressed: _generateJavaScriptCode,
                child: Text('Generate JavaScript Code'),
              ),
            ],
          ),
          Expanded(
            child: DataTable(
              columns: [
                DataColumn(label: Text('Indicator/Operator')),
                ...List.generate(columns.length, (index) {
                  return DataColumn(
                    label: Row(
                      children: [
                        Text(columns[index]),
                        DropdownButton<String>(
                          value: columnTypes[index],
                          items: ['Indicator', 'Operator', 'Number','Video'].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          onChanged: (value) {
                            setState(() {
                              columnTypes[index] = value!;
                            });
                          },
                        ),
                      ],
                    ),
                  );
                }),
              ],
              rows: List.generate(widget.selectedIndicators.length, (rowIndex) {
                return DataRow(
                  cells: [
                    DataCell(Text(widget.selectedIndicators[rowIndex])),
                    ...List.generate(columns.length, (colIndex) {
                      if (columnTypes[colIndex] == 'Number') {
                        return DataCell(
                          Row(
                            children: [
                              IconButton(
                                icon: Icon(Icons.remove),
                                onPressed: () => _changeMatrixInput(rowIndex,colIndex, -1),
                              ),
                              Text(matrix.getValue(rowIndex, colIndex).toString()),
                              IconButton(
                                icon: Icon(Icons.add),
                                onPressed: () => _changeMatrixInput(rowIndex,colIndex, 1),
                              ),
                            ],
                          ),
                        );
                      } else {
                        return DataCell(
                          DropdownButton<String?>(
                            value: columnValues[colIndex].length > rowIndex ? columnValues[colIndex][rowIndex] : null,
                            hint: Text('Select ' + columnTypes[colIndex]),
                            items: columnTypes[colIndex] == 'Indicator'
                                ? widget.selectedIndicators.map<DropdownMenuItem<String?>>((indicator) {
                              return DropdownMenuItem<String?>(
                                value: indicator,
                                child: Text(indicator),
                              );
                            }).toList()
                                : columnTypes[colIndex] == 'Operator'
                                ? widget.selectedOperators.map<DropdownMenuItem<String?>>((operator) {
                              return DropdownMenuItem<String?>(
                                value: operator,
                                child: Text(operator),
                              );
                            }).toList()
                                : columnTypes[colIndex] == 'Video'
                                ? videos.map<DropdownMenuItem<String?>>((video) {
                              return DropdownMenuItem<String?>(
                                value: video,
                                child: Text(video),
                              );
                            }).toList()
                                : [], // Handle other cases as needed
                            onChanged: (value) {
                              setState(() {
                                if (columnValues[colIndex].length <= rowIndex) {
                                  columnValues[colIndex].add(value);
                                } else {
                                  columnValues[colIndex][rowIndex] = value;
                                }
                              });
                            },
                          ),
                        );

                      }
                    }),
                  ],
                );
              }),
            ),
          ),
        ],
      ),
    );
  }
}
